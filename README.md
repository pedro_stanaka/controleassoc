# OpenQuota - Association Management with ease

This web application manages an association and has the following features:

 * Management of associates
 * Management of departments 
 * Management of events (with newsletter via e-mail)
 * Management of payments
 * Management of employees/volunteers
 * System self-management (level of access, permissions, theme settings)


## Requirements

 * Apache 2+ (with mod_rewrite enabled)
 * Mysql
 * PHP 5.5+


## Installation

The easiest way of installing it is by downloading the latest release and unzip under some directory/sub-directory of your server root and 
then accessing http://localhost/path/to/dir/install.

Then follow the steps shown in screen.




